#!/bin/bash
python manage.py makemigrations
python manage.py migrate
#python manage.py collectstatic --noinput
exec circusd circus.ini --log-level debug

exec "$@";